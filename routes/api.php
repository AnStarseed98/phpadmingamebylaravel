<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('nguoi-choi', 'API\NguoiChoiController@getAllUser');
Route::get('cap-nhat-nguoi-choi', 'API\NguoiChoiController@updateInfo');
Route::post('check-login', 'API\CheckLoginController@checkLogin');
Route::get('check-register', 'API\CheckRegisterController@checkRegister');
Route::post('check-exist', 'API\NguoiChoiController@checkForgotPass');
Route::post('get-new-pass', 'API\NguoiChoiController@updatePassword');

Route::post('manager', 'API\NguoiChoiController@managerUser');

Route::any('facebook-chatbot', 'API\FacebookChatbotController@index');
