@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Danh sách câu hỏi</h2>
                <a href="{{ route('capnhatcauhoi') }}" style="margin-bottom: 10px" class="btn btn-info waves-effect waves-light">
                    <i class="mdi mdi-plus mr-1"></i> Thêm mới
                </a>
                <div class="card">
                    <div class="card-body">
                        <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nội dung</th>
                                <th>ID lĩnh vực</th>
                                <th>Phương án A</th>
                                <th>Phương án B</th>
                                <th>Phương án C</th>
                                <th>Phương án D</th>
                                <th>Đáp án</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $v)
                            <tr>
                                @if($v->xoa)

                                    @foreach($linhvuc as $k => $value)
                                        @if($v->linh_vuc_id == $value->id)
                                            <td>{{$v->id}}</td>
                                            <td>{{$v->noi_dung}}</td>
                                            <td>{{$value->ten_linh_vuc}}</td>
                                            <td>{{$v->phuong_an_a}}</td>
                                            <td>{{$v->phuong_an_b}}</td>
                                            <td>{{$v->phuong_an_c}}</td>
                                            <td>{{$v->phuong_an_d}}</td>
                                            <td>{{$v->dap_an}}</td>
                                            <td>
                                                <a href="{{route('capnhatcauhoi',['id' => $v->id])}}" title="edit" class="btn btn-success waves-effect waves-light">
                                                    <i class="mdi mdi-pencil"></i>
                                                </a>
                                                <a href="{{route('xoacauhoi',['id' => $v->id])}}" title="del" class="btn btn-danger waves-effect waves-light">
                                                    <i class="mdi mdi-trash-can-outline"></i>
                                                </a>
                                            </td>
                                        @endif
                                    @endforeach

                                @endif
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
    </div>

@endsection