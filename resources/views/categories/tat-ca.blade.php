@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Danh sách lĩnh vực</h2>
                <a href="{{ route('capnhatlinhvuc') }}" style="margin-bottom: 10px" class="btn btn-info waves-effect waves-light">
                    <i class="mdi mdi-plus mr-1"></i> Thêm mới
                </a>
                <div class="card">
                    <div class="card-body">
                        <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                            <tr>
                                <th>Mã</th>
                                <th>Tên lĩnh vực</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $v)
                            <tr>
                                @if(1 == $v->xoa)
                                    <td>{{$v->id}}</td>
                                    <td>{{$v->ten_linh_vuc}}</td>
                                    <td>
                                        <a href="{{route('capnhatlinhvuc',['id' => $v->id])}}" title="edit" class="btn btn-success waves-effect waves-light">
                                            <i class="mdi mdi-pencil"></i>
                                        </a>
                                        <a href="{{route('xoalinhvuc',['id' => $v->id])}}" title="del" class="btn btn-danger waves-effect waves-light">
                                            <i class=" mdi mdi-trash-can-outline"></i>
                                        </a>
                                    </td>
                                @endif
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
    </div>

@endsection