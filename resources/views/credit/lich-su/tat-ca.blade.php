@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Lịch sử mua credit</h2>
                <div class="card">
                    <div class="card-body">
                        <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>ID người chơi</th>
                                    <th>ID gói credit</th>
                                    <th>Credit</th>
                                    <th>Số tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $v)
                                <tr>
                                    <td>{{$v->id}}</td>
                                    <td>{{$v->nguoi_choi_id}}</td>
                                    <td>{{$v->goi_credit_id}}</td>
                                    <td>{{$v->credit}}</td>
                                    <td>{{$v->so_tien}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
    </div>

@endsection