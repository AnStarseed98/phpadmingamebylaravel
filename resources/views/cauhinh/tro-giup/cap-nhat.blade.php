@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Thêm mới/Cập nhật câu hỏi</h2>
                <div class="card-box">
                    <form method="post" action="{{route('capnhathelp',['id' => $id,'type' => true])}}" class="parsley-examples">
                        @csrf
                        <div class="form-group">
                            <label for="loai_tro_giup">Loại trợ giúp</label>
                            <input type="text" name="loai_tro_giup" parsley-trigger="change" required
                                   value="{{$loai_tro_giup}}" disabled class="form-control" id="loai_tro_giup" style="width: 25%">
                        </div>
                        <div class="form-group">
                            <label for="credit">credit</label>
                            <input type="text" name="credit" parsley-trigger="change" required
                                   value="{{$credit}}" class="form-control" id="credit" style="width: 25%">
                        </div>

                        <div class="form-group mb-0">
                            <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-secondary waves-effect">
                                Cancel
                            </button>
                        </div>

                    </form>
                </div> <!-- end card-box -->
            </div>
            <!-- end col -->
        </div>
    </div>
@endsection
