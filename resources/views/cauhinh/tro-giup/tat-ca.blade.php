@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Lịch sử mua credit</h2>
                <div class="card">
                    <div class="card-body">
                        <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Loại trợ giúp</th>
                                    <th>Thứ tự</th>
                                    <th>Credit</th>
                                    <th>Số tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $v)
                                <tr>
                                    <td>{{$v->id}}</td>
                                    <td>{{$v->loai_tro_giup}}</td>
                                    <td>{{$v->thu_tu}}</td>
                                    <td>{{$v->credit}}</td>
                                    <td>
                                        <a href="{{route('capnhathelp',['id' => $v->id])}}" title="edit" class="btn btn-success waves-effect waves-light">
                                            <i class="mdi mdi-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
    </div>

@endsection