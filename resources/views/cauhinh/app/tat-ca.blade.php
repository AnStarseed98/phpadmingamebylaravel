@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Lịch sử mua credit</h2>
                <div class="card">
                    <div class="card-body">
                        <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Cơ hội sai</th>
                                    <th>Thời gian trả lời</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $v)
                                <tr>
                                    <td>{{$v->id}}</td>
                                    <td>{{$v->co_hoi_sai}}</td>
                                    <td>{{$v->thoi_gian_tra_loi}}</td>
                                    <td>
                                        <a href="{{route('capnhatapp',['id' => $v->id])}}" title="edit" class="btn btn-success waves-effect waves-light">
                                            <i class="mdi mdi-pencil"></i>
                                        </a>
                                        <a href="{{route('xoaapp',['id' => $v->id])}}" title="del" class="btn btn-danger waves-effect waves-light">
                                            <i class="mdi mdi-trash-can-outline"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
    </div>

@endsection