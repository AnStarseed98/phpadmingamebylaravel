@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Thêm mới/Cập nhật câu hỏi</h2>
                <div class="card-box">
                    <form method="post" action="{{route('capnhatdiem',['id' => $id,'type' => true])}}" class="parsley-examples">
                        @csrf
                        <div class="form-group">
                            <label for="thu_tu">Thứ tự</label>
                            <input type="text" name="thu_tu" parsley-trigger="change" required
                                   value="{{$thu_tu}}" disabled class="form-control" id="thu_tu" style="width: 25%">
                        </div>
                        <div class="form-group">
                            <label for="diem">Điểm</label>
                            <input type="text" name="diem" parsley-trigger="change" required
                                   value="{{$diem}}" class="form-control" id="diem" style="width: 25%">
                        </div>

                        <div class="form-group mb-0">
                            <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-secondary waves-effect">
                                Cancel
                            </button>
                        </div>

                    </form>
                </div> <!-- end card-box -->
            </div>
            <!-- end col -->
        </div>
    </div>
@endsection
