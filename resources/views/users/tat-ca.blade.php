@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Danh sách người chơi</h2>
                <a href="{{route('capnhatnguoichoi')}}" style="margin-bottom: 10px" class="btn btn-info waves-effect waves-light">
                    <i class="fe-user-plus mr-1"></i> Thêm người chơi
                </a>
                <a href="{{route('listdel')}}" style="margin-bottom: 10px" class="btn btn-info waves-effect waves-light">
                    <i class="fe-user-plus mr-1"></i> Thùng rác
                </a>
                <div class="card">
                    <div class="card-body">
                        <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên đăng nhập</th>
                                <th>Email</th>
                                <th>Hình đại diện</th>
                                <th>Điểm cao nhất</th>
                                <th>Credit</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $v)
                            <tr>
                                    <td>{{$v->id}}</td>
                                    <td>{{$v->ten_dang_nhap}}</td>
                                    <td>{{$v->email}}</td>
                                    <td>{{$v->hinh_dai_dien}}</td>
                                    <td>{{$v->diem_cao_nhat}}</td>
                                    <td>{{$v->credit}}</td>
                                    <td>
                                        @if(!empty($type))
                                            <a href="{{route('xoanguoichoi',['id' => $v->id,'type' => 1])}}" title="Khôi phục" class="btn btn-success waves-effect waves-light">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{route('xoanguoichoi',['id' => $v->id,'type' => 3])}}" title="del" class="btn btn-danger waves-effect waves-light">
                                                <i class="mdi mdi-trash-can-outline"></i>
                                            </a>
                                        @else
                                            <a href="{{route('capnhatnguoichoi',['id' => $v->id])}}" title="edit" class="btn btn-success waves-effect waves-light">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                            <a href="{{route('xoanguoichoi',['id' => $v->id])}}" title="del" class="btn btn-danger waves-effect waves-light">
                                                <i class="mdi mdi-trash-can-outline"></i>
                                            </a>
                                        @endif
                                    </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
    </div>

@endsection