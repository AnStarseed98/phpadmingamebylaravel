@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Chi tiết lượt chơi</h2>
                <div class="card">
                    <div class="card-body">
                        <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ID lượt chơi</th>
                                <th>ID câu hỏi</th>
                                <th>Phương án</th>
                                <th>Điểm</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $v)
                                <tr>
                                    <td>{{$v->id}}</td>
                                    <td>{{$v->luot_choi_id}}</td>
                                    <td>{{$v->cau_hoi_id}}</td>
                                    <td>{{$v->phuong_an}}</td>
                                    <td>{{$v->diem}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
    </div>

@endsection