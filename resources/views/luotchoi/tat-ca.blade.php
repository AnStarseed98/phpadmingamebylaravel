@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Danh sách lượt chơi</h2>
                <div class="card">
                    <div class="card-body">
                        <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ID người chơi</th>
                                <th>Số câu</th>
                                <th>Điểm</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $v)
                            <tr>
                                <td>{{$v->id}}</td>
                                <td>{{$v->nguoi_choi_id}}</td>
                                <td>{{$v->so_cau}}</td>
                                <td>{{$v->diem}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
    </div>

@endsection