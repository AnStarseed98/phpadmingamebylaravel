<header id="topnav">
    <!-- Topbar Start -->
    <div class="navbar-custom">
        <div class="container-fluid">
            <ul class="list-unstyled topnav-menu float-right mb-0">

                <li class="dropdown notification-list">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle nav-link">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </li>

                <li class="d-none d-sm-block">
                    <form class="app-search">
                        <div class="app-search-box">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search...">
                                <div class="input-group-append">
                                    <button class="btn" type="submit">
                                        <i class="fe-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </li>

                <!-- Authentication Links -->
                @guest
                    <li class="dropdown notification-list">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                @endguest
            </ul>

            <!-- LOGO -->
            <div class="logo-box">
                <a href="index.html" class="logo text-center">
                    <span class="logo-lg">
                        <img src="assets/images/logo-dark.png" alt="" height="26">
                    </span>
                    <span class="logo-sm">
                        <img src="assets/images/logo-sm.png" alt="" height="28">
                    </span>
                </a>
            </div>

            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">

                <li class="dropdown d-none d-lg-block">
                    <h2 class="nav-link  waves-effect" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                        Admin
                    </h2>
                </li>


            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- end Topbar -->

    <div class="topbar-menu">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="{{ route('tatcanguoichoi') }}">
                            <i class="la la-user"></i>
                            Người chơi
                        </a>
                    </li>

                    <li class="has-submenu">
                        <a href="{{ route('tatcalinhvuc') }}">
                            <i class=" la la-bookmark-o"></i>
                            Lĩnh vực
                        </a>
                    </li>

                    <li class="has-submenu">
                        <a href="{{route('tatcacauhoi')}}">
                            <i class="la la-question"></i>
                            Câu hỏi
                        </a>
                    </li>

                    <li class="has-submenu">
                        <a href="#"> <i class=" la la-diamond"></i>Credit <div class="arrow-down"></div></a>
                        <ul class="submenu">
                            <li>
                                <a href="{{route('tatcacredit')}}">Danh sách</a>
                            </li>
                            <li>
                                <a href="{{route('tatcacredithistory')}}">Lịch sử mua credit</a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"> <i class=" la la-gamepad"></i>Lượt chơi<div class="arrow-down"></div></a>
                        <ul class="submenu">
                            <li>
                                <a href="{{route('tatcaluotchoi')}}">Danh sách</a>
                            </li>
                            <li>
                                <a href="{{route('tatcachitietluotchoi')}}">Chi tiết lượt chơi</a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class="la la-edit"></i>Cấu hình<div class="arrow-down"></div></a>
                        <ul class="submenu">
                            <li>
                                <a href="{{route('cauhinhapp')}}">App</a>
                            </li>
                            <li>
                                <a href="{{route('diemcauhoi')}}">Điểm câu hỏi</a>
                            </li>
                            <li>
                                <a href="{{route('trogiupcauhoi')}}">Trợ giúp</a>
                            </li>
                        </ul>
                    </li>

{{--                    <li class="has-submenu">--}}
{{--                        <a href="#"> <i class="la la-user-secret"></i>Quản trị viên<div class="arrow-down"></div></a>--}}
{{--                        <ul class="submenu">--}}
{{--                            <li>--}}
{{--                                <a href="#">Danh sách</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#">Thêm mới/Chỉnh sửa</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}

                </ul>
                <!-- End navigation menu -->

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</header>
    <div class="wrapper">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    
                </div>
            </div>
            <!-- end page title -->


        </div> <!-- end container -->
    </div>