<?php

namespace App\Http\Controllers;

use App\NguoiChoi;
use Illuminate\Http\Request;


class NguoiChoiController extends Controller
{
    public function index(NguoiChoi $user)
    {
        $data = $user->findAllUser();

        return view('users.tat-ca',
            [
                'data' => $data,
            ]
        );
    }

    public function capnhatNguoiChoi(Request $request, NguoiChoi $user)
    {
        $data = [];
        if (!empty($request->id))
            $data = $user->findUserbyId($request->id);

        return view('users.cap-nhat',
            ['data' => $data]
        );
    }

    public function del(Request $request, NguoiChoi $user)
    {
        if (!empty($request->id)) {
            !empty($request->type) ? $user->delUser($request->id,$request->type) : $user->delUser($request->id);
        }

        return redirect(route('tatcanguoichoi'));
    }

    public function check(Request $request, NguoiChoi $user)
    {
        $data = $request->all();
        if (!($user->checkExists('ten_dang_nhap', $request->ten_dang_nhap))) {
            $this->__updateInfo($data, $user);
            return redirect(route('tatcanguoichoi'));
        }

        return back();
    }

    private function __updateInfo($data, NguoiChoi $user)
    {
        $user->updateInfo($data);
    }

    public function listdel(NguoiChoi $user)
    {
        $data = $user->findAllUser(2);

        //        dd($data);
        return view('users.tat-ca',
            [
                'type' => 'trash',
                'data' => $data,
            ]
        );
    }
}
