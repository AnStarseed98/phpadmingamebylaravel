<?php

namespace App\Http\Controllers;

use App\DiemCauHoi;
use App\TroGiup;
use Illuminate\Http\Request;

class CauHinhController extends Controller
{
    protected $getPointQuestion;
    protected $getHelpQuestion;

    public function __construct()
    {
        $this->getPointQuestion = new DiemCauHoi();
        $this->getHelpQuestion  = new TroGiup();
    }

    public function pointQuestion()
    {
        $data = $this->getPointQuestion::all();

        return view('cauhinh/diem-cau-hoi.tat-ca',
            ['data' => $data]
        );
    }

    public function updatePoint(Request $request)
    {
        $data = "don't have value";

        if (!empty($request->id)) {
            $data = $this->getPointQuestion::find($request->id);
            if (!empty($request->type)) {
                $data->diem = $request->diem;
                $data->save();

                return $this->pointQuestion();
            }
        }

        return view('cauhinh/diem-cau-hoi.cap-nhat', [
                'id'     => $data->id,
                'thu_tu' => $data->thu_tu,
                'diem'   => $data->diem,
            ]
        );
    }

    public function helpQuestion()
    {
        $data = $this->getHelpQuestion::all();

        return view('cauhinh/tro-giup.tat-ca',
            ['data' => $data]
        );
    }

    public function updateHelp(Request $request)
    {
        $data = "don't have value";

        if (!empty($request->id)) {
            $data = $this->getHelpQuestion::find($request->id);
            if (!empty($request->type)) {
                $data->credit = $request->credit;
                $data->save();

                return $this->helpQuestion();
            }
        }

        return view('cauhinh/tro-giup.cap-nhat', [
                'id'            => $data->id,
                'loai_tro_giup' => $data->loai_tro_giup,
                'credit'        => $data->credit,
            ]
        );
    }

    public function app()
    {
        return 'chua lam';
    }
}
