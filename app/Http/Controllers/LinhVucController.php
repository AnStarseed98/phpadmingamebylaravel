<?php

namespace App\Http\Controllers;

use App\LinhVuc;
use Illuminate\Http\Request;

class LinhVucController extends Controller
{
    public function index(LinhVuc $linhvuc)
    {
        $data = $linhvuc->findAll();
        //dd($data);
        return view('categories.tat-ca',
            ['data' => $data]
        );
    }

    public function capnhatLinhVuc(Request $request, LinhVuc $linhvuc)
    {
        $data = [];
        if (!empty($request->id))
            $data = $linhvuc->findLinhVuc($request->id);
        return view('categories.cap-nhat',
            ['data' => $data]
        );
    }

    public function del(Request $request, LinhVuc $linhvuc)
    {
        if (!empty($request->id))
            $linhvuc->del($request->id);
//        dd($linhvuc);
        return redirect(route('tatcalinhvuc'));
    }

    public function check(Request $request, LinhVuc $linhvuc)
    {
        $data = $request->all();//dd($data);
        $linhvuc->updateInfo($data);
        return redirect(route('tatcalinhvuc'));
    }
}
