<?php

namespace App\Http\Controllers\Chatbot\Models;

use App\DiemCauHoi;
use App\LichSuMuaCredit;
use App\NguoiChoi;
use App\TroGiup;
use Illuminate\Database\Eloquent\Model;

class SaveInfo extends Model
{

    protected $dataUser     = [];
    protected $dataCredit   = [];
    protected $tableUser    = [];
    protected $listHelp     = [];
    protected $listPoint    = [];

    public function __construct()
    {
        $this->listHelp    = new TroGiup();
        $this->listPoint   = new DiemCauHoi();
        $this->dataCredit  = new LichSuMuaCredit();
        $this->tableUser   = new NguoiChoi();
    }

    public function __checkUser($key, $userId)
    {
        $this->dataUser = NguoiChoi::where($key, $userId)->where('xoa', 1)->first();
        if (!empty($this->dataUser)) {
            $this->dataUser = $this->dataUser->toArray();

            return true;
        }

        return false;
    }

    public function __getUserId($k, $v)
    {
        return $this->tableUser->returnId($k, $v);
    }

    public function __getCreditId($k, $v)
    {
        //        /
    }

    public function __getUser()
    {
        return $this->dataUser;
    }

    public function __getListPoint()
    {
        foreach ($this->listPoint::all() as $key => $value) {
            $valuePoint[$value->thu_tu] = $value->diem;
        }

        return $valuePoint;
    }

    public function __getListValueCredit()
    {
        foreach ($this->listHelp::all() as $key => $value) {
            switch ($value->loai_tro_giup) {
                case 'Chơi lại':
                    $valueCredit['play_again'] = $value->credit;
                    break;
                case 'Bỏ qua':
                    $valueCredit['ignore'] = $value->credit;
                    break;
                default:
                    break;
            }
        }

        return $valueCredit;
    }

    public function __setUser($key, $userId)
    {
        $this->dataUser = NguoiChoi::where($key, $userId)->where('xoa', 1)->first()->toArray();
    }

    public function __createAccount($id)
    {
        $user = new NguoiChoi();
        $user->fill([
            'ten_dang_nhap' => $id,
            'mat_khau'      => null,
            'email'         => $id . '@gmail.com',
            'hinh_dai_dien' => null,
            'diem_cao_nhat' => 0,
            'credit'        => 0,
            'id_facebook'   => $id,
        ]);

        $user->save();

        return $user;
    }

    public function __createAndroid($where, $key, $value)
    {
        $result = NguoiChoi::where('id_facebook', $where)->update([
            $key => $value,
        ]);

        return $result;
    }

    public function __saveCredit($idUser, $idCredit, $credit, $money)
    {

        $this->dataCredit::insert([
            'nguoi_choi_id' => $idUser,
            'goi_credit_id' => $idCredit,
            'credit'        => $credit,
            'so_tien'       => $money,
        ]);
    }

    public function __updateByData($id, $data){
        // upgrade update, better than __createAndroid
        $result = [];
        foreach ($data as $key => $value) {
            $result[$key] = NguoiChoi::where('id_facebook', $id)->update([$key => $value]);
        }

        return $result;
    }

    public function __showRecord($page = 1, $limit = 1)
    {

        $nguoiChoi = $this->tableUser::select('ten_dang_nhap', 'diem_cao_nhat', 'credit')->where('xoa', 1)->orderBy('diem_cao_nhat', 'desc')->skip(($page - 1) * $limit)->take($limit)->get();

        return $result = [
            'count' => $this->tableUser::where('xoa', 1)->count(),
            'data'  => $nguoiChoi,
        ];
    }

}