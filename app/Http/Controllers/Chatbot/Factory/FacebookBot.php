<?php

namespace App\Http\Controllers\chatbot\Factory;

use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Cache\LaravelCache;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Facebook\FacebookDriver;
use BotMan\Drivers\Facebook\FacebookLocationDriver;


class FacebookBot extends ConnectWebhook
{

    private $config;

    private $token;

    private $app_secret;

    private $verification;

    protected $botman;

    public function __construct($token, $app_secret, $verification)
    {
        $this->app_secret   = $app_secret;
        $this->token        = $token;
        $this->verification = $verification;

        $this->config = [
            "facebook" => [
                "token"        => $this->token,
                'app_secret'   => $this->app_secret,
                'verification' => $this->verification,

            ],
            "botman"           => [
                "conversation_cache_time" => 60 * 24 * 30 * 12 * 3,
                "user_cache_time"  => 60 * 24 * 30 * 12 * 3,
            ],
        ];
        parent::__construct($this->verification);
        parent::verifyWebhook();
        // Load the driver(s) you want to use

        DriverManager::loadDriver(FacebookDriver::class);
        DriverManager::loadDriver(FacebookLocationDriver::class);

    }

    public function createBotman()
    {
        $this->botman = BotManFactory::create($this->config,new LaravelCache());

        return $this->botman;
    }


}
