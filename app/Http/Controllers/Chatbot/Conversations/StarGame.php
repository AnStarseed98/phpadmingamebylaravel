<?php

namespace App\Http\Controllers\Chatbot\Conversations;


use App\Http\Controllers\Chatbot\Models\SaveInfo;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class StarGame extends Conversation
{
    protected $dataUser = [];
    protected $entityManager;
    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;
        $this->entityManager  = new SaveInfo();
    }


    public function run()
    {
        // This will be called immediately
        $this->cacheTime = 30;
        if (!empty($this->entityManager->__checkUser('id_facebook', $this->bot->getUser()->getId()))) {
            return $this->haveAccount();
        }

        return $this->unHaveAccount();
    }

    public function haveAccount()
    {
        $question = Question::create('Bạn đã có tài khoản, bạn có muốn: ')
            ->addButtons([
                Button::create('Xem thông tin')->value('show_info'),
                Button::create('Bắt đầu trò chơi')->value('star_game'),
                Button::create('Kết thúc')->value('end'),
            ]);
        $this->ask($question, function(Answer $answer) {
            switch ($answer->getText()) {
                case 'Xem thông tin' :
                    return $this->showInfo();
                case 'Bắt đầu trò chơi':
                    $this->bot->startConversation(new StarQuestion($this->entityManager->__getUser()['diem_cao_nhat'], $this->entityManager->__getUser()['credit']));
                    break;
                case 'Kết thúc'  :
                    return $this->endConversation();
                default           :
                    return $this->repeat();
            }
        });
    }

    public function showInfo()
    {
        $this->say(
            'Điểm cao nhất của bạn: ' . $this->entityManager->__getUser()['diem_cao_nhat'] . '
Gói credit của bạn: ' . $this->entityManager->__getUser()['credit']
        );
        if ($this->entityManager->__getUser()['ten_dang_nhap'] === $this->entityManager->__getUser()['id_facebook']) {
            $question = Question::create('Bạn chưa có tài khoản cho android. Tạo ngay? ')
                ->addButtons([
                    Button::create('Tạo tài khoản')->value('create_android'),
                    Button::create('Kết thúc')->value('end'),
                ]);
            $this->ask($question, function(Answer $answer) {
                switch ($answer->getValue()) {
                    case 'create_android' :
                        return $this->askUsername();
                    case 'end'  :
                        return $this->endConversation();
                    default           :
                        return $this->repeat();
                }
            });
        } else {

            $question = Question::create('Tên tài khoản của bạn: ' . $this->entityManager->__getUser()['ten_dang_nhap'] . '
Mail của bạn: ' . $this->entityManager->__getUser()['email'])
                ->addButtons([
                    Button::create('Mua credit')->value('buy_credit'),
                    Button::create('Xem kỷ lục')->value('view_record'),
                    Button::create('Chơi game')->value('star_game'),
                    Button::create('Kết thúc')->value('end'),
                ]);
            $this->ask($question, function(Answer $answer) {
                switch ($answer->getValue()) {
                    case 'buy_credit' :
                        $this->bot->startConversation(new StarQuestion($this->entityManager->__getUser()['diem_cao_nhat'], $this->entityManager->__getUser()['credit'],'showcredit'));
                        break;
                    case 'view_record' :
                        $this->bot->startConversation(new ShowRecord());
                        break;
                    case 'star_game' :
                        $this->bot->startConversation(new StarQuestion($this->entityManager->__getUser()['diem_cao_nhat'], $this->entityManager->__getUser()['credit']));
                        break;
                    case 'end'  :
                        return $this->endConversation();
                    default           :
                        return $this->repeat();
                }
            });
        }


    }

    public function askUsername($text = 'Hãy nhập username: ')
    {
        $this->ask($text, function(Answer $answer) {

            if ($this->entityManager->__checkUser('ten_dang_nhap', $answer->getText())) {
                return $this->askUsername('Username đã tồn tại, mời nhập lại: ');
            } else {
                $this->say('Thành công');
                $this->askPassword();
                $this->entityManager->__createAndroid($this->bot->getUser()->getId(), 'ten_dang_nhap', $answer->getText());
            }
        });
    }

    public function askPassword($text = 'Hãy nhập password: ')
    {
        $this->ask($text, function(Answer $answer) {

            $this->say('Thành công, password của bạn là: ' . $answer->getText());
            $this->say('Để đảm bảo an toàn, hãy xóa tin nhắn chứa password');
            $this->entityManager->__createAndroid($this->bot->getUser()->getId(), 'mat_khau', $answer->getText());
            return $this->askEmail();

        });
    }

    public function askEmail($text = 'Hãy nhập email: ')
    {
        $this->ask($text, function(Answer $answer) {

            if (filter_var($answer->getText(), FILTER_VALIDATE_EMAIL)) {
                if ($this->entityManager->__checkUser('email', $answer->getText()))
                    return $this->askEmail('email đã tồn tại, mời nhập lại: ');

                $this->say('Bạn đã tạo tài khoản thành công, bạn có thể dùng nó để đăng nhập trong ứng dụng android!');
                $this->entityManager->__createAndroid($this->bot->getUser()->getId(), 'email', $answer->getText());
                return $this->run();
            }
            return $this->askEmail('email sai định dạng, mời nhập lại: ');
        }
        );
    }

    public function unHaveAccount()
    {
        $question = Question::create('Bạn chưa có tài khoản, bạn có muốn: ')
            ->addButtons([
                Button::create('Tạo tài khoản')->value('create_account'),
                Button::create('Kết thúc')->value('end'),
            ]);
        $this->ask($question, function(Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {

                //check when user click btn in messenger
                switch ($answer->getValue()) {
                    case 'create_account' :
                        if (!empty($this->createAccount())) {
                            $this->say('tạo thành công');
                            $this->haveAccount();
                        } else {
                            $this->say('tạo thất bại');
                            $this->endConversation();
                        }
                        break;
                    case 'end'        :
                        return $this->endConversation();
                    default           :
                        return $this->$this->repeat();
                }
            }

            return $this->$this->repeat();
        });
    }

    public function createAccount()
    {
        $user = $this->entityManager->__createAccount($this->bot->getUser()->getId());

        return $user;
    }

    public function createAndroid($where, $key, $value)
    {

        $user = $this->entityManager->__createAndroid($where, $key, $value); //update user where id_facebook = $where into table = $key and value = $value
        return $user;
    }

    public function endConversation()
    {
        return $this->say('Trò chơi kết thúc!');
    }
}
