<?php

namespace App\Http\Controllers\Chatbot\Conversations;


use App\Http\Controllers\Chatbot\Models\SaveInfo;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class ShowRecord extends Conversation
{
    protected $entityManager;
    protected $limit = 2;

    public function __construct()
    {
        $this->entityManager = new SaveInfo();
    }

    public function run()
    {
        $this->showRecord();
    }

    public function showRecord($page = 1,$limit = 1)
    {
        $data = $this->entityManager->__showRecord($page,$limit);

        $i = 1;
        $text = 'Name _ Điểm _ Credit';
        foreach ($data['data'] as $key => $value) {
            $text = $text . '
' . $i++ . '. ' . $value['ten_dang_nhap'] . ' _ ' . $value['diem_cao_nhat'] . ' _ ' . $value['credit'];
        }

        $this->say('Danh sách người chơi đạt điểm cao ('.$data['count'].' người): ');

        $question = Question::create($text)
            ->addButtons([
                Button::create('Xem thêm')->value('show_more_record'),
                Button::create('Kết thúc')->value('end'),
            ]);

        $this->ask($question,function(Answer $answer) {
            switch ($answer->getText()) {
                case 'Xem thêm' :
                    return $this->showRecord(1,$this->limit++);
                case 'Kết thúc'  :
                    return $this->bot->startConversation(new StarGame());
                default           :
                    return $this->repeat();
            }
        });
    }
}