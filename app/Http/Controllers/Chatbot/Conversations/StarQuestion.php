<?php

namespace App\Http\Controllers\Chatbot\Conversations;

use App\CauHoi;
use App\GoiCredit;
use App\Http\Controllers\Chatbot\Models\SaveInfo;
use App\LinhVuc;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class StarQuestion extends Conversation
{
    public $type;

    public $totalPoint;
    public $totalCredit;
    public $help50 = true; //remove 2 answer false
    public $ignore = true; //ignore question
    public $ratio  = true; //ratio question

    public $valueCredit;
    public $pointQuestion;

    protected $listCategories    = [];
    protected $arrCategories     = []; // arr have list categories

    protected $listCredit        = [];
    protected $textInfoCredit    = []; // string have info credit
    protected $btnCredit         = []; // button credit

    protected $entityManager;
    protected $dataUser          = [];

    protected $listQuestion      = [];
    protected $quesIndex         = 0; // index of question
    protected $quesContent       = []; // content of question in Index


    public function __construct($point, $credit, $type = null)
    {
        $this->type = $type;

        $this->listCategories    = LinhVuc::select('ten_linh_vuc', 'id')->where('xoa', 1)->get();
        $this->listCredit        = GoiCredit::where('xoa', 1)->take(9)->get();

        $this->entityManager     = new SaveInfo();

        $this->valueCredit       = $this->entityManager->__getListValueCredit();
        $this->pointQuestion     = $this->entityManager->__getListPoint();

        $this->totalPoint  = $point;
        $this->totalCredit = $credit;
    }

    public function run()
    {
        // This will be called immediately
        $this->cacheTime = 24 * 60;
        if ($this->type === 'showcredit')
            return $this->showCredit();

        return $this->askTopic();
    }

    public function askTopic()
    {

        foreach ($this->listCategories as $key => $value) {
            $this->arrCategories[] = $value['ten_linh_vuc']; // array have list linhvuc
            $btnCat[]              = Button::create($value['ten_linh_vuc'])->value($value['id']); //array button facebook
        }
        $question = Question::create('Chọn lĩnh vực câu hỏi')
            ->addButtons($btnCat);
        $this->ask($question, function(Answer $answer) {

            if ($answer->isInteractiveMessageReply()) {
                //check when user click btn in messenger
                if (in_array($answer->getText(), $this->arrCategories)) {
                    // when mess in list linhvuc

                    $this->__dectectQuestion(CauHoi::all()->where('linh_vuc_id', $answer->getValue())->where('xoa', 1)->toArray()); //get answer by linhvuc
                    $this->starQuestion();
                } else {
                    //when false
                    $this->say($answer->getText() . ' không phù hợp, nhập lại');
                    $this->repeat();
                }
            } else {
                // when user text in mess
                $this->say($answer->getText() . ' không phù hợp, hãy nhập lại');
                $this->repeat();
            }
        });
    }

    public function starQuestion($indexQuestion = 0, $type = null)
    {
        if (empty($this->listQuestion[$indexQuestion])) {
            $this->say('Câu hỏi trong lĩnh vực này hiện đang chưa có!');
            $this->say('in Controllers\Chatbot\Conversations\StarQuestion.php -> 103');
            return $this->askTopic();
        }

        $this->quesIndex++; // index of answer

        $this->quesContent = $this->listQuestion[$indexQuestion]; //array have data of answer
        // add btn answer in mess
        $btnAnswer[] = Button::create($this->quesContent['phuong_an_a'])->value($this->quesContent['phuong_an_a']);
        $btnAnswer[] = Button::create($this->quesContent['phuong_an_b'])->value($this->quesContent['phuong_an_b']);
        $btnAnswer[] = Button::create($this->quesContent['phuong_an_c'])->value($this->quesContent['phuong_an_c']);
        $btnAnswer[] = Button::create($this->quesContent['phuong_an_d'])->value($this->quesContent['phuong_an_d']);

        if ($type == 50) {
            $del = 0; // remove 2 answer
            foreach ($btnAnswer as $key => $value) {
                if ($value->toArray()['value'] !== $this->quesContent['dap_an']) {
                    unset($btnAnswer[$key]);
                    $del++;
                }
                if ($del == 2)
                    break;
            }
        }

        if ($type === 'ratio') {
            $a = rand(0,100);
            $b = rand(0,100-$a);
            $c = rand(0,100-$a-$b);
            $d = 100-$a-$b-$c;
            $arrRatio = [$a,$b,$c,$d];

            foreach ($btnAnswer as $key => $value) {
                if ($value->toArray()['value'] !== $this->quesContent['dap_an']) {
                    $btnAnswer[$key] = Button::create(min($arrRatio).'% : '.$value->toArray()['value'])->value($value->toArray()['value']);
                    unset($arrRatio[array_search(min($arrRatio), $arrRatio)]);
                } else {
                    $btnAnswer[$key] = Button::create(max($arrRatio).'% : '.$value->toArray()['value'])->value($value->toArray()['value']);
                    unset($arrRatio[array_search(max($arrRatio), $arrRatio)]);
                }
            }
        } else {
            if (sizeof($btnAnswer) > 2 && $this->ratio)
                $btnAnswer[] = Button::create('Hỏi ý kiến khán giả')->value('ratio_game');
        }

        if ($this->help50) {
            $btnAnswer[] = Button::create('Trợ giúp 50-50')->value('remove_answer');
        }

        if (($this->ignore) && ($this->quesIndex < sizeof($this->listQuestion))) {
            $btnAnswer[] = Button::create('Bỏ qua')->value('ignore_game');
        }

        $btnAnswer[] = Button::create('Dừng game')->value('stop_game');
        $question    = Question::create('Câu ' . $this->quesIndex . ': ' . $this->quesContent['noi_dung'])
            ->addButtons($btnAnswer);

        $this->ask($question, function(Answer $answer) {

            switch ($answer->getValue()) {
                case 'remove_answer' :
                    $this->quesIndex--;
                    $this->help50 = false;

                    return $this->__help('50');
                case 'ratio_game' :
                    $this->quesIndex--;
                    $this->ratio = false;

                    return $this->__help('ratio');
                case 'ignore_game' :
                    $this->ignore = false;

                    return $this->starQuestion($this->quesIndex);
                case 'stop_game' :
                    return $this->endConversation();
                case $this->quesContent['dap_an'] :
                    if (!empty($this->pointQuestion[$this->quesIndex])) {
                        $this->totalPoint += $this->pointQuestion[$this->quesIndex];
                    } else {
                        return $this->say('chua co diem cau hoi, check Chatbot\Conversations\StarQuestion.php -> 184');
                    }

                    $this->say('nice!');

                    //when user end of list question
                    if ($this->quesIndex == sizeof($this->listQuestion)) {
                        //save point
                        $this->entityManager->__updateByData($this->bot->getUser()->getId(), ['diem_cao_nhat' => $this->totalPoint]);
                        $this->say('your point: '. $this->totalPoint);
                        return $this->say('Bạn đã hoàn thành tất cả câu hỏi. Xin chúc mừng!');
                    }

                    //next question
                    return $this->starQuestion($this->quesIndex);
                default :
                    $this->quesIndex--;
                    return $this->answerFalse();
            }
        });
    }

    private function __dectectQuestion(array $listQuestion)
    {
        foreach ($listQuestion as $k => $v) {
            $this->listQuestion[] = $v;
        }

        return $this->listQuestion;
    }

    private function __help($text)
    {
        switch ($text) {
            case '50' :
                return $this->starQuestion($this->quesIndex, '50');
            case 'ratio' :
                return $this->starQuestion($this->quesIndex, 'ratio');
            default :
                return $this->starQuestion($this->quesIndex);
        }
    }

    public function answerFalse()
    {
        $question = Question::create('Câu trả lời không chính xác, bạn có thể mua hoặc sử dụng credit để chơi lại! Giá ' . $this->valueCredit['play_again'] . 'credit/lượt!')
            ->addButtons([
                Button::create('Mua lượt')->value('play_again'),
                Button::create('Kết thúc')->value('end'),
            ]);
        $this->ask($question, function(Answer $answer) {

            if ($answer->isInteractiveMessageReply()) {

                //check when user click btn in messenger
                switch ($answer->getValue()) {
                    case 'play_again' :
                        return $this->buyCredit();
                    case 'end'        :
                        return $this->endConversation();
                    default           :
                        $this->say($answer->getText() . ' không phù hợp, nhập lại');

                        return $this->repeat();
                }
            } else {
                $this->say($answer->getText() . ' không phù hợp, nhập lại');

                return $this->repeat();
            }


        });

        //return $this->say('Trò chơi kết thúc!');
    }

    public function buyCredit()
    {
        if ($this->totalCredit >= $this->valueCredit['play_again']) {
            $this->say('Mua lượt thành công. Chơi lại nào!');
            $this->totalCredit = $this->totalCredit - $this->valueCredit['play_again'];
            $this->entityManager->__updateByData($this->bot->getUser()->getId(), ['credit' => $this->totalCredit]);

            return $this->starQuestion($this->quesIndex);
        }

        return $this->nohaveCredit();
    }

    public function nohaveCredit()
    {
        $question = Question::create('Bạn đang không đủ credit, Mua Ngay!')
            ->addButtons([
                Button::create('Xem các gói')->value('view_credit'),
                Button::create('Kết thúc')->value('end'),
            ]);
        $this->ask($question, function(Answer $answer) {

            if ($answer->isInteractiveMessageReply()) {

                //check when user click btn in messenger
                switch ($answer->getValue()) {
                    case 'view_credit' :
                        return $this->showCredit();//$this->starQuestion($this->quesIndex);
                    case 'end'        :
                        return $this->endConversation();
                    default           :
                        $this->say($answer->getText() . ' không phù hợp, nhập lại');

                        return $this->repeat();
                }
            } else {
                $this->say($answer->getText() . ' không phù hợp, nhập lại');

                return $this->repeat();
            }
        });
    }

    public function showCredit()
    {
        $str = 'Các gói credit: ';
        foreach ($this->listCredit as $k => $v) {
            $str = $str . '
' . $v['ten_goi'] . ' : ' . $v['so_tien'] . 'k => ' . $v['credit'] . ' credit';

            $this->btnCredit[] = Button::create($v['ten_goi'])->value($v['credit']); //array button facebook
        };
        $this->btnCredit[] = Button::create('Kết thúc')->value('end');
        $question          = Question::create($str)
            ->addButtons($this->btnCredit);
        $this->ask($question, function(Answer $answer) {

            if ($answer->isInteractiveMessageReply()) {

                //check when user click btn in messenger
                switch ($answer->getText()) {
                    case 'Gói thường' :
                        return $this->doneBuyCredit('bạn vừa mua gói thường, số credit của bạn là: ', (int) $answer->getValue());
                    case 'Gói vip'        :
                        return $this->doneBuyCredit('bạn vừa mua gói vip, số credit của bạn là: ', (int) $answer->getValue());
                    case 'Kết thúc'        :
                        return $this->bot->startConversation(new StarGame());
                    default           :
                        $this->say($answer->getText() . ' không phù hợp, nhập lại');

                        return $this->repeat();
                }
            } else {
                $this->say($answer->getText() . ' không phù hợp, nhập lại');

                return $this->repeat();
            }


        });
    }

    public function doneBuyCredit(string $text, int $value)
    {

        $this->totalCredit = $this->totalCredit + $value;

        $this->entityManager->__updateByData($this->bot->getUser()->getId(), ['credit' => $this->totalCredit]);

        $idUser = $this->entityManager->__getUserId('id_facebook', $this->bot->getUser()->getId());
        $credit = GoiCredit::where('credit', $value)->where('xoa', 1)->first();

        $this->entityManager->__saveCredit($idUser, $credit->id, $value, $credit->so_tien);

        $this->say($text . $this->totalCredit);
        if ($this->type === 'showcredit')
            return $this->bot->startConversation(new StarGame('donebuycredit'));

        return $this->buyCredit();
    }

    public function endConversation()
    {
        $this->entityManager->__updateByData($this->bot->getUser()->getId(), ['diem_cao_nhat' => $this->totalPoint]);
        $this->say('your point: '. $this->totalPoint);
        return $this->say('Trò chơi kết thúc!');
    }
}
