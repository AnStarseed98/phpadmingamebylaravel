<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\NguoiChoi;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class CheckRegisterController extends Controller
{
    public function checkRegister(Request $request)
    {
        try {

            $dataUser = [
                'ten_dang_nhap' => $request->ten_dang_nhap,
                'mat_khau'      => $request->mat_khau,
                'email'         => $request->email,
                'hinh_dai_dien' => null,
                'diem_cao_nhat' => 0,
                'credit'        => 0,
            ];

            $user = new NguoiChoi();
            //            dd(!empty($user->updateInfo($dataUser)));
            if (!($user->checkExists('ten_dang_nhap', $request->ten_dang_nhap))) {
                if (!empty($user->updateInfo($dataUser)))
                    return response()->json(['success' => true, 'notifi' => 'Đăng kí thành công!!']);

                return response()->json(['success' => false, 'notifi' => 'Đăng kí không thành công']);
            } else {
                return response()->json(['success' => false, 'notifi' => 'Tài khoản đã được đăng ký']);
            }

        } catch (Exception $e) {
            return response()->json(['success' => false, 'notifi' => 'Không lấy được dữ liệu']);
        }
    }
}
