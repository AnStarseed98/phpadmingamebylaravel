<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NguoiChoi;
use Illuminate\Support\Facades\Hash;
use mysql_xdevapi\Exception;

class CheckLoginController extends Controller
{
    public function checkLogin(Request $request) {
        try {

            $nguoiChoi = NguoiChoi::where('ten_dang_nhap',$request->ten_dang_nhap)->first();
            if (!empty($nguoiChoi)) {
                if ($request->mat_khau === $nguoiChoi->mat_khau)
                    return response()->json(['success' => true,'data' => $nguoiChoi]);
            }
            return response()->json(['success' => false, 'notifi' => 'Thông tin sai']);

        } catch (Exception $e)
        {
            return response()->json(['success' => false, 'notifi' => 'Không lấy được dữ liệu']);
        }
    }
}
