<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\NguoiChoi;
use Illuminate\Http\Request;

class NguoiChoiController extends Controller
{
    public function getAllUser(Request $request)
    {
        try {
            $data = $request->data;

            !empty($data['page'])  ? $page  = $data['page']  : $page  = 1;
            !empty($data['limit']) ? $limit = $data['limit'] : $limit = 25;

            $page  = $request->query('page', $page);
            $limit = $request->query('limit', $limit);

            $result    = [
                'success' => false,
                'result'  => null,
            ];
//            $ran =  NguoiChoi::all()->random(5);
            $nguoiChoi = NguoiChoi::select('ten_dang_nhap', 'email', 'hinh_dai_dien', 'diem_cao_nhat', 'credit')->orderBy('diem_cao_nhat', 'desc')->skip(($page - 1) * $limit)->take($limit)->get()->random();

            if (!empty($nguoiChoi)) {
                $result = [
                    'success' => NguoiChoi::count(),
                    'result'  => $nguoiChoi,
                ];
            }

            return response()->json($result);
        } catch (\Exception $e) {
            $result = [
                'success' => false,
                'result'  => 'error',
            ];

            return response()->json($result);
        }

    }

    public function updateInfo(Request $request)
    {
        try {
            $data = $request->data;
            $user = $request->ten_dang_nhap;
            //            $dataUpdate = [
            //                'ten_dang_nhap' => $request->ten_dang_nhap,
            //                'data'          => [
            //                    'mat_khau'      => 'PASS',
            //                    'email'         => 'email',
            //                    'hinh_dai_dien' => 'hinh_dai_dien',
            //                ]
            //            ];
            //dd(json_encode($dataUpdate));
            if (!empty($user)) {
                $result = new NguoiChoi();

                return response()->json($result->updateUserBy($user, $data));
            }

            return response()->json(['result' => 'missing username']);
        } catch (\Exception $e) {
            return response()->json(['result' => 'false']);
        }
    }

    public function checkForgotPass(Request $request) {
        $nguoiChoi = NguoiChoi::where('ten_dang_nhap', $request->ten_dang_nhap)->first();
        if ($nguoiChoi != null) {
            if ($nguoiChoi->email == $request->email) {
                return response()->json(['success' => true]);
            } else {
                return response()->json(['success' => false, 'notifi' => 'Email không chính xác']);
            }
        }
        else {
            return response()->json(['success' => false, 'notifi' => 'Tên đăng nhập không tồn tại']);
        }
    }

    public function updatePassword(Request $request) {
        try {
            $nguoiChoi = NguoiChoi::where('ten_dang_nhap', $request->ten_dang_nhap)->first();
            $nguoiChoi->mat_khau = $request->mat_khau;
            $nguoiChoi->save();
            return response()->json(['success' => true, 'notifi' => 'Đổi mật khẩu thành công']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'notifi' => 'Đổi mật khẩu thất bại']);
        }
    }

    public function managerUser(Request $request) {
        $nguoiChoi = NguoiChoi::where('ten_dang_nhap', $request->ten_dang_nhap)->first();
        if ($nguoiChoi != null) {
            $nguoiChoi = $nguoiChoi->update($request->all());
            return response()->json(['success' => true, 'notifi' => 'Thay đổi thông tin thành công']);
        } else {
            return response()->json(['success' => false, 'notifi' => 'Thay đổi thông tin thất bại']);
        }
    }
}