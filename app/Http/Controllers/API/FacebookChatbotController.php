<?php

namespace App\Http\Controllers\API;

use App\CauHoi;
use App\DiemCauHoi;
use App\GoiCredit;
use App\Http\Controllers\Chatbot\Conversations\StarGame;
use App\Http\Controllers\Chatbot\Models\SaveInfo;
use App\Http\Controllers\Controller;
use App\LichSuMuaCredit;
use App\LinhVuc;
use App\NguoiChoi;
use App\TroGiup;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;



class FacebookChatbotController extends Controller {

    protected $facebookbot;
    protected $botman;
    protected $botManEvents;

    const VERIFY_WEBHOOK_TOKEN  = 'go';
    const APP_SECRET            = '3f42368961df5b621ae5f118c119fe54';
    const PAGE_ACCESS_TOKEN     = 'EAAJwcol5nskBAPrbZCfoHERWLZBquVTIWqRRlm6YVmUNZADqGZACwfOn7mitvSENLnLcGtDiJyDfLyVCUydcF95R1NEMiGFgrJcP6Y6AfHcKKRDsk6ZACpKBOxyKft0Gykh7ZBRjECMVeFcPTAZAIHxQU8qh3rrXrdwqFwovOYqD0V0KM0PmYu5';

    public function __construct()
    {
        $this->facebookbot   = new \App\Http\Controllers\chatbot\Factory\FacebookBot(self::PAGE_ACCESS_TOKEN, self::APP_SECRET, self::VERIFY_WEBHOOK_TOKEN);
        $this->botman        = $this->facebookbot->createBotman();
    }

    public function index(){
        $botman = $this->botman;

        $listStart = [
            'Get Started',
            'Hello',
            'Hi',
            'News',
            'Start',
            'Bắt Đầu',
        ];

        //star chatbot
        foreach ($listStart as $key => $value) {
            $botman->hears($value, function(BotMan $bot) {
                $user = $bot->getUser();
                $bot->reply(ButtonTemplate::create(
                    'Chào mừng bạn '.$user->getFirstName().' '.$user->getLastName().' đến với game ai là triệu phú. Bạn cần giúp đỡ gì ạ'
                ) //can't use getUserName()
                ->addButton(ElementButton::create('Chơi ngay')
                    ->type('postback')
                    ->payload('play_now')
                )
                    ->addButton(ElementButton::create('Vấn đề khác')
                        ->type('postback')
                        ->payload('chat_admin')
                    )
                );
            });
        }

        $listStop = [
            'stop',
            'chat_admin',
        ];

        //stop
        foreach ($listStop as $key => $value) {
            $botman->hears($value, function(BotMan $bot) {

                $bot->reply(ButtonTemplate::create(
                    'End:'
                )
                    ->addButton(ElementButton::create('Tìm hiểu thêm ')
                        ->url('https://apollo.edu.vn/tieng-anh-tre-em?utm_source=Facebook&utm_medium=Chatbot&utm_campaign=tieng-anh-thieu-nhi')
                    ));
            })->stopsConversation();
        }

        $botman->hears('play_now',function(BotMan $bot){
            $bot->startConversation(new StarGame());
        });

        $botman->listen();die;
    }
}