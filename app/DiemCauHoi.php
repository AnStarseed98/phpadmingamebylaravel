<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiemCauHoi extends Model
{
    protected $table = 'cauhinhdiemcauhoi';
    protected $fillable = ['diem'];
    protected $guarded = 'id';

}
