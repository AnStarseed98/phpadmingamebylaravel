<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoiCredit extends Model
{
    protected $table = 'goicredit';
    protected $guarded = ['id'];

    public function updateInfo($data)
    {

        !empty($data['credit'])? $credit = $data['credit'] : $credit = '';
        !empty($data['so_tien'])? $money = $data['credit'] : $money = '';

        if ($credit < 0)
            $credit = 0;
        if ($money < 0)
            $money = 0;

        $flight = self::updateOrCreate(
            [
                'id' => $data['id'],
            ],
            [
                'ten_goi' => $data['ten_goi'],
                'credit'  => $credit,
                'so_tien' => $money,
                'xoa'     => 1,
            ]
        );
    }

    public function delUser($id)
    {
        $cre      = self::find($id);
        $cre->xoa = '2';
        $cre->save();
    }
}
