<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinhVuc extends Model
{
    protected $table = 'linhvuc';

    protected $guarded = ['id'];

    public function findLinhVuc($id)
    {
        return self::find($id);
    }

    public function findAll()
    {
        return self::all()->where('xoa',1);
    }

    public function updateInfo($data)
    {

        $flight = self::updateOrCreate(
            [
                'id' => $data['id'],
            ],
            [
                'ten_linh_vuc' => $data['ten_linh_vuc'],
                'xoa'          => 1,
            ]
        );
    }

    public function del($id)
    {
        $linhvuc      = $this->find($id);
        $linhvuc->xoa = '2';
        $linhvuc->save();
    }
}
