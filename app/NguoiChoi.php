<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NguoiChoi extends Model
{
    protected $table = 'nguoichoi';
    protected $fillable = ['ten_dang_nhap','mat_khau','email','hinh_dai_dien','diem_cao_nhat','credit','xoa','id_facebook'];
    protected $guarded = ['id'];

    public function findUserbyId($id)
    {
        return self::find($id);
    }

    public function returnId($key,$value) {
        return self::select('id')->where($key,$value)->where('xoa',1)->first()->id;
    }

    public function findAllUser($type = 1)
    {
        return self::all()->where('xoa', $type);
    }

    public function updateInfo($data)
    {
        !empty($data['id'])             ? $id       = $data['id']                   : $id     = '';
        !empty($data['id_facebook'])    ? $idfb     = $data['id_facebook']          : $idfb     = null;
        !empty($data['credit'])         ? $credit   = $data['credit']               : $credit   = 0;
        !empty($data['diem_cao_nhat'])  ? $point    = $data['diem_cao_nhat']        : $point    = 0;
        !empty($data['hinh_dai_dien'])  ? $image    = $data['hinh_dai_dien']        : $image    = null;

        $flight = self::updateOrCreate(
            [
                'id' => $id,
            ],
            [
                'ten_dang_nhap' => $data['ten_dang_nhap'],
                'mat_khau'      => $data['mat_khau'],
                'email'         => $data['email'],
                'hinh_dai_dien' => $image,
                'diem_cao_nhat' => $point,
                'credit'        => $credit,
                'id_facebook'   => $idfb,
                'xoa'           => 1,
            ]
        );

        return $flight;
    }

    public function delUser($id,$type = 2)
    {
        $user      = $this->findUserbyId($id);
        $user->xoa = $type;
        $user->save();
    }

    public function luotChoi()
    {
        return $this->belongsToMany('App\LuotChoi');
    }

    public function lichsumuacredit()
    {
        return $this->hasMany('App\LichSuMuaCredit');
    }

    public function checkExists($info, $value)
    {
        if (self::where($info, $value)->exists())
            return true;

        return false;
    }

    public function updateUserBy($user, $data)
    {

        $flight = [];
        foreach ($data as $key => $value) {
            $flight[$key] = self::where('ten_dang_nhap', $user)->update([$key => $value]);
        }

        return $flight;
    }
}
